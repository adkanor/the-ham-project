const images = {
  "section-work_img1": {
    class: "work__tabs-info graphic",
    sublitle: "Graphic Design",
  },
  "section-work_img2": {
    class: "work__tabs-info graphic",
    sublitle: "Graphic Design",
  },
  "section-work_img3": {
    class: "work__tabs-info web",
    sublitle: "Web Design",
  },
  "section-work_img4": {
    class: "work__tabs-info web",
    sublitle: "Web Design",
  },
  "section-work_img5": {
    class: "work__tabs-info graphic",
    sublitle: "Graphic Design",
  },
  "section-work_img6": {
    class: "work__tabs-info wordpress",
    sublitle: "Wordpress",
  },
  "section-work_img7": {
    class: "work__tabs-info landing",
    sublitle: "Landing",
  },
  "section-work_img8": {
    class: "work__tabs-info wordpress",
    sublitle: "Wordpress",
  },
  "section-work_img9": {
    class: "work__tabs-info landing",
    sublitle: "Landing",
  },
  "section-work_img10": {
    class: "work__tabs-info wordpress",
    sublitle: "Wordpress",
  },
  "section-work_img11": {
    class: "work__tabs-info wordpress",
    sublitle: "Wordpress",
  },
  "section-work_img12": {
    class: "work__tabs-info landing",
    sublitle: "Landing",
  },
};
