"use strict";
//....................Tab switches to the services section

const tabsServices = document.querySelectorAll(".services__container .tabs");
console.log(tabsServices);

tabsServices.forEach((tab) => {
  tab.addEventListener("click", tabChange);
});

function tabChange({ target }) {
  const currentTab = target.closest(".services__tabs-title");
  if (!currentTab) return;
  const tabsContainer = currentTab.closest(".services__container");
  const activeTabId = currentTab.dataset.tabId;
  const toggleActiveState = (element) => {
    element.dataset.tabId !== activeTabId
      ? element.classList.remove("active")
      : element.classList.add("active");
  };
  tabsContainer.querySelectorAll(".services__tabs-title").forEach((tab) => {
    toggleActiveState(tab);
  });
  tabsContainer.querySelectorAll(".services__tabs-info").forEach((content) => {
    toggleActiveState(content);
  });
}
//.....................Uploading additional photos with click in work section
const listOfImages = document.querySelector(".work__tabs-content");
const loadingButton = document.querySelector(".work__button-loading");
let loader = document.querySelector(".loader");
let countOfLoading = 0;
function uploadingPhotos() {
  loader.style.display = "none";
  if (countOfLoading == 2) {
    loadingButton.style.display = "none";
  } else {
    for (let key in images) {
      let listItem = document.createElement("li");
      listItem.classList = images[key]["class"];
      let img = document.createElement("img");
      img.src = "images/" + key + ".png";
      img.alt = key;
      listOfImages.append(listItem);
      listItem.append(img);

      let listOwelayInfo = document.createElement("div");
      listOwelayInfo.classList.add("work__owerlay-content");
      let listOwelayIcons = document.createElement("img");
      listOwelayIcons.classList.add("work__owerlay-img");
      listOwelayIcons.src = "./images/section-work_owerlay-icons.png";
      listOwelayIcons.alt = "work_owerlay-icons";
      let listOwelayTitle = document.createElement("h4");
      listOwelayTitle.textContent = "Creative design";
      listOwelayTitle.classList.add("work__owerlay-title");
      let listOwelaySubtitle = document.createElement("h4");
      listOwelaySubtitle.classList.add("work__owerlay-subtitle");
      listOwelaySubtitle.textContent = images[key]["subtitle"];
      listOwelayInfo.prepend(listOwelayIcons);
      listOwelayInfo.append(listOwelayTitle);
      listOwelayInfo.append(listOwelaySubtitle);
      listItem.append(listOwelayInfo);
    }
  }
  countOfLoading++;
}
loadingButton.addEventListener("click", (event) => {
  event.preventDefault();
  loader.style.display = "block";

  setTimeout(uploadingPhotos, 2000);
});
//..................... Filter for tabs in work section

let tabsWork = document.querySelector(".work__tabs");

tabsWork.addEventListener("click", (event) => {
  let tabsImages = document.querySelectorAll(".work__tabs-info");

  let result = event.target;
  console.log(result);
  let filterClass = event.target.dataset["tabKey"];
  console.log(filterClass);
  if (filterClass === undefined) {
    return false;
  }
  tabsImages.forEach((elem) => {
    elem.classList.remove("hide");
    if (!elem.classList.contains(filterClass) && filterClass !== "all") {
      elem.classList.add("hide");
    }
  });
});
//..................... Icon switcher for  opinion section
let personsOpinionSection = document.querySelector(".opinion__list");
personsOpinionSection.addEventListener("click", showPersonOpinion);

let personComent = document.querySelector(".opinion__coment");
let personName = document.querySelector(".opinion__persons-name");
let personJob = document.querySelector(".opinion__persons-job");
let personImage = document.querySelector(".opinion__img");
let currentTab;
function showPersonOpinion(event) {
  if (currentTab !== undefined) {
    currentTab.classList.remove("active");
  }
  currentTab = event.target;
  currentTab.classList.add("active");
  addInfo();
}

//..................... Universal arrow icon switcher for optiona section
const leftArrow = document.querySelector(".arrow-left");
leftArrow.addEventListener("click", (event) => {
  directionMove(true);
});
const rightArrow = document.querySelector(".arrow-right");
rightArrow.addEventListener("click", (event) => {
  directionMove(false);
});

function directionMove(isLeft) {
  if (currentTab == undefined) {
    currentTab = personsOpinionSection.firstElementChild;
  } else if (isLeft && currentTab.previousElementSibling == null) {
    currentTab.classList.remove("active");
    currentTab = personsOpinionSection.lastElementChild;
  } else if (!isLeft && currentTab.nextElementSibling == null) {
    currentTab.classList.remove("active");
    currentTab = personsOpinionSection.firstElementChild;
  } else if (currentTab !== undefined && currentTab !== null) {
    currentTab.classList.remove("active");
    currentTab = isLeft
      ? currentTab.previousElementSibling
      : currentTab.nextElementSibling;
  }
  addInfo();
  currentTab.classList.add("active");
}

// Add info about people for opinion section:
function addInfo() {
  let data = currentTab.dataset["keyOption"];
  if (data === undefined) {
    return false;
  }
  personComent.textContent = info[data]["coment"];
  personName.textContent = info[data]["name"];
  personJob.textContent = info[data]["job"];
  personImage.setAttribute("src", info[data]["img"]);
}
// Added images to gallery section
let imagesToAdd = document.querySelector(".gallery__container");
function createGalleryImages() {
  for (let key in galleryOfImages) {
    let img = document.createElement("img");
    img.src = "images/" + key + ".png";
    img.alt = key;
    img.classList.add("gallery__image");

    let div = document.createElement("div");
    div.classList.add("gallery__div");
    div.append(img);
    imagesToAdd.append(div);
  }
}
createGalleryImages();

// Creating gallery with Masonry

function initMasonry() {
  let gallery = document.querySelector(".gallery__container");
  new Masonry(gallery, {
    itemSelector: ".gallery__div",
    gutter: 15,
    columnWidth: 1,
  });
}

// initMasonry();

setTimeout(initMasonry, 100);

// Adding more photos to  gallery

let loadButton = document.querySelector(".gallery__button-loading");
let loadingCount = 0;
loadButton.addEventListener("click", (e) => {
  e.preventDefault();
  if (loadingCount == 1) {
    loadButton.style.display = "none";
    return false;
  }

  createGalleryImages();
  setTimeout(initMasonry, 100);
  loadingCount++;
});
