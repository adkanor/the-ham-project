const info = {
  Julia: {
    coment:
      "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
    name: "Julia Helsin",
    job: "Web designer",
    img: "./images/section-opinion_pers1.png",
  },
  Hasan: {
    coment:
      "Integerm, non dictum odio nisi quis sa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
    name: "Hasan Ali",
    job: "UX designer",
    img: "./images/section-opinion_pers2.png",
  },
  Emir: {
    coment:
      "Integer digam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
    name: "Emir Katowskiy",
    job: "Java Developer",
    img: "./images/section-opinion_pers3.png",
  },
  Lidia: {
    coment:
      "Integer dignissim, augue tempus uaoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
    name: "Lidia Freimut",
    job: "FrontEnd developer",
    img: "./images/section-opinion_pers4.png",
  },
};
